# AssoConnect - Technical interview

This app is a minimal blogging system accessible via a REST API. The application allows to manage :
- Users
- Articles
- Comments

The project is based on a minimal installation of Symfony 5.
You can use any relevant vendor.

The instructions will not be very precise on purpose, so that you can do what you feel is the most important according to you.
